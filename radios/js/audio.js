(function() {
	var audio = document.getElementById('audio');

	$('#audio__play').on('click', function() {
		$(this).fadeOut(600, function() {
			$('#pause').fadeIn(600);
		});
		audio.play()	
	});

	$('#pause').on('click', function() {
		$(this).fadeOut(600, function() {
			$('#audio__play').fadeIn(600);
		});
		audio.pause()	
	});

})();